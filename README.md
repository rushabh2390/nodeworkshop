Pre-prerequisite module to work with this node js
- npm install table
- npm install csv-parse

7 programs in node js which uses csv-parse,table and  fs modules to read csv file and perform the certain task. It will print Result in Tabular format

7 programs description are given below.
program 1: Read the listing.csv file and print its content is object array mode. 
    example node prog1.js

program 2: Read the listing.csv file and print places's details (i.e. Name, price, street,has_availability and instant_bookable)  for which instant_bookable is true. Here instant_bookable=true is passed as Command Line Argument (CLI).
    example node prog2.js "instant_bookable=true"

program 3: Read the listing.csv file and print places's details (i.e. Name, price, street,has_availability ,property_type, amenities)  for which property_type is 'House'. Here property_type=House pass as CLI.
    example node prog3.js "property_type=House"

program 4: Read the listing.csv file and print places's details (i.e. Name, price, street,has_availability ,property_type, amenities)  for which amenities have "Laptop Friendly Workspace" amenities=Laptop Friendly Workspace is pass as CLI.
    example node prog4.js "amenities=Laptop Friendly Workspace"

program 5: Read the listing.csv file and print places's details (i.e. Name, price, availability_30, amenities)  for which prices is greater than 0 and less than 200. Here 0 < price < 200 pass as CLI. 
    example node prog5.js '0 < price < 200'

program 6: Read the listing.csv file and print  first 20 places's details (i.e. Name, street, lat, long, price)  for which latitude and longitude  is nearer to given latitude and longitude is pass as CLI. 
    example node prog6.js "42.38" "-71.13"

program 7: Read the listing.csv and reviews.csv file and print places's details(i.e. Name, price, reviewer_name, comments ) for which listing_id is "11008495". here listing_id pass as CLI to get place's name and price from listing.csv and review_name and comments taken from reviews.csv 
    example node prog7.js "11008495"